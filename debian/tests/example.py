#! /usr/bin/python3
# This is the example from upstream README file.  It may need adjustments in
# case of api changes.  The original script is modified to be more verbose, and
# to finish in a finite time; the original script shows a window open until
# someone closes manually the window.
import glfw

def main():
    print("Initialize the library")
    if not glfw.init():
        return 1
    print("Create a windowed mode window and its OpenGL context")
    window = glfw.create_window(640, 480, "Hello World", None, None)
    if not window:
        glfw.terminate()
        return 2

    print("Make the window's context current")
    glfw.make_context_current(window)

    print("Loop until the user closes the window or a certain number of")
    print("iterations has passed, for unattended tests")
    i = 0
    while i < 8 and not glfw.window_should_close(window):
        print("Render here, e.g. using pyOpenGL")
        i = i + 1
        print("(iteration " + str(i) + ")")

        print("Swap front and back buffers")
        glfw.swap_buffers(window)

        print("Poll for and process events")
        glfw.poll_events()

    print("Terminate the library")
    glfw.terminate()
    return 0

if __name__ == "__main__":
    exit(main())
