python-pyglfw (2.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.8.0+dfsg
  * d/salsa-ci.yml: disable i386 Salsa CI builds.
    i386 builds never passed and are generating unnecessary noise.
  * d/t/run-example: set XDG_RUNTIME_DIR.
    This fixes autopkgtest issues caused by a message about
    XDG_RUNTIME_DIR environment variable being unset.
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Wed, 18 Dec 2024 11:39:31 +0100

python-pyglfw (2.7.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.7.0+dfsg

 -- Étienne Mollier <emollier@debian.org>  Wed, 28 Feb 2024 10:34:39 +0100

python-pyglfw (2.6.5+dfsg-1) unstable; urgency=medium

  * New upstream version
  * d/copyright: bump copyright years.

 -- Étienne Mollier <emollier@debian.org>  Tue, 30 Jan 2024 21:06:41 +0100

python-pyglfw (2.6.4+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Étienne Mollier <emollier@debian.org>  Wed, 27 Dec 2023 17:00:32 +0100

python-pyglfw (2.6.3+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Étienne Mollier <emollier@debian.org>  Sun, 26 Nov 2023 12:14:01 +0100

python-pyglfw (2.6.2+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Étienne Mollier <emollier@debian.org>  Mon, 10 Jul 2023 23:29:34 +0200

python-pyglfw (2.5.9+dfsg-1) unstable; urgency=medium

  * New upstream version 2.5.9+dfsg

 -- Étienne Mollier <emollier@debian.org>  Sun, 11 Jun 2023 11:45:12 +0200

python-pyglfw (2.5.6+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Add salsa-ci file (routine-update)

 -- Étienne Mollier <emollier@debian.org>  Sat, 04 Feb 2023 15:29:20 +0100

python-pyglfw (2.5.5+dfsg-4) unstable; urgency=medium

  * d/t/autopkgtest-pkg-python.conf: also exclude archs for autodep8.

 -- Étienne Mollier <emollier@debian.org>  Sat, 10 Dec 2022 12:39:38 +0100

python-pyglfw (2.5.5+dfsg-3) unstable; urgency=medium

  * d/control: add X dependencies for tests at build time.
  * d/rules: execute the d/t/example.py script at build time test.
  * d/t/control: exclude architectures with broken glfw3 for now.

 -- Étienne Mollier <emollier@debian.org>  Thu, 08 Dec 2022 22:17:55 +0100

python-pyglfw (2.5.5+dfsg-2) unstable; urgency=medium

  * Source only upload.

 -- Étienne Mollier <emollier@debian.org>  Mon, 05 Dec 2022 22:20:05 +0100

python-pyglfw (2.5.5+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #1025413)

 -- Étienne Mollier <emollier@debian.org>  Sun, 04 Dec 2022 16:26:07 +0100
